California Pools is one of the largest pool builders in the country and brings award-winning custom pools to the Riverside area. As seen on Pool Kings, we offer expert design solutions and innovative construction techniques to provide our customers with the highest-quality pool or backyard anywhere.

Address: 23846 Sunnymead Blvd, #7, Moreno Valley, CA 92553, USA

Phone: 951-795-4090

Website: https://californiapools.com/locations/riverside
